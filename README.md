**This Plugin is discontinued. See [admin-tools-gui](https://gitlab.com/fabian.kramer/admin-tools-gui) for further updates**

English:

This plugin simply shows some important information about your server.

 

Permissions:

serverstats.use -- use /serverstats

 

Commands:

/serverstats -- Show the info